package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.time.Instant;

public class PostDaoTests {

    private JdbcTemplate mockJdbc;
    private PostDAO postDao;
    private Post post;

    @BeforeEach
    public void init_PostDao() {
        Timestamp creationTime = Timestamp.from(Instant.now());
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        postDao = new PostDAO(mockJdbc);
        // target post
        post = new Post("some author", creationTime, "some forum",
                "some message", -1, -1, false);
        post.setId(2077);
        // data mocking
        Post postClone = new Post("some author", creationTime, "some forum",
                "some message", -1, -1, false);
        postClone.setId(2077);
        Mockito.when(mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(postClone.getId())
        )).thenReturn(postClone);
    }

    @Test
    // not a part of test suite
    public void test_setPost_Null() {
        Assertions.assertThrows(NullPointerException.class, () -> PostDAO.setPost(2078, post));
    }

    @Test
    public void test_setPost_None() {
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(post.getId()));
    }

    @Test
    public void test_setPost_Author() {
        post.setAuthor("author");
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(post.getAuthor()),
                Mockito.eq(post.getId()));
    }

    @Test
    public void test_setPost_Message() {
        post.setMessage("message");
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(post.getMessage()),
                Mockito.eq(post.getId()));
    }

    @Test
    public void test_setPost_Created() {
        post.setCreated(Timestamp.from(Instant.now().plusSeconds(3600)));
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(post.getCreated()),
                Mockito.eq(post.getId()));
    }

    @Test
    public void test_setPost_Author_and_Message() {
        post.setAuthor("author");
        post.setMessage("message");
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(post.getAuthor()),
                Mockito.eq(post.getMessage()),
                Mockito.eq(post.getId()));
    }

    @Test
    public void test_setPost_Author_and_Created() {
        post.setAuthor("author");
        post.setCreated(Timestamp.from(Instant.now().plusSeconds(3600)));
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(post.getAuthor()),
                Mockito.eq(post.getCreated()),
                Mockito.eq(post.getId()));
    }

    @Test
    public void test_setPost_Message_and_Created() {
        post.setMessage("message");
        post.setCreated(Timestamp.from(Instant.now().plusSeconds(3600)));
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(post.getMessage()),
                Mockito.eq(post.getCreated()),
                Mockito.eq(post.getId()));
    }

    @Test
    public void test_setPost_Author_and_Message_and_Created() {
        post.setAuthor("author");
        post.setMessage("message");
        post.setCreated(Timestamp.from(Instant.now().plusSeconds(7200)));
        PostDAO.setPost(2077, post);
        Mockito.verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(post.getAuthor()),
                Mockito.eq(post.getMessage()),
                Mockito.eq(post.getCreated()),
                Mockito.eq(post.getId()));
    }

    @AfterEach
    public void destroy_PostDao() {

    }
}
