package com.hw.db.DAO;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ForumDaoTests {

    private JdbcTemplate mockJdbc;
    private ForumDAO forumDAO;

    private void verify_UserList(String sqlQuery) {
        Mockito.verify(mockJdbc).query(
                Mockito.eq(sqlQuery),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @BeforeEach
    public void init_ForumDao() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        forumDAO = new ForumDAO(mockJdbc);
    }

    @Test
    public void test_UserList_noSince_DescFalse() {
        ForumDAO.UserList(null, null, null, false);
        verify_UserList("SELECT nickname,fullname,email,about " +
                "FROM forum_users " +
                "WHERE forum = (?)::citext " +
                "ORDER BY nickname;");
    }

    @Test
    public void test_UserList_DescTrue() {
        ForumDAO.UserList(null, 1000, "since", true);
        verify_UserList("SELECT nickname,fullname,email,about " +
                "FROM forum_users " +
                "WHERE forum = (?)::citext AND  nickname < (?)::citext " +
                "ORDER BY nickname desc " +
                "LIMIT ?;");
    }

    @Test
    public void test_UserList_DescFalse() {
        ForumDAO.UserList(null, 1000, "since", false);
        verify_UserList("SELECT nickname,fullname,email,about " +
                "FROM forum_users " +
                "WHERE forum = (?)::citext AND  nickname > (?)::citext " +
                "ORDER BY nickname " +
                "LIMIT ?;");
    }


    @AfterEach
    public void destroy_ForumDao() {
        mockJdbc = null;
        forumDAO = null;
    }
}
