package com.hw.db.DAO;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ThreadDaoTests {

    private JdbcTemplate mockJdbc;
    private ThreadDAO threadDao;

    @BeforeEach
    public void init_ThreadDao() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        threadDao = new ThreadDAO(mockJdbc);
    }

    @Test
    public void test_ThreadDao_DescFalse() {
        ThreadDAO.treeSort(65536, 16384, 32768, false);
    }

    @Test
    public void test_ThreadDao_DescTrue() {
        ThreadDAO.treeSort(65536, 16384, 32768, true);
    }

    @AfterEach
    public void destroy_ThreadDao() {
        mockJdbc = null;
        threadDao = null;
    }
}
