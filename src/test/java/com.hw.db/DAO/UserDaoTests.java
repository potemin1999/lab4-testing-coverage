package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDaoTests {

    private JdbcTemplate mockJdbc;
    private UserDAO userDao;
    private User user;

    @BeforeEach
    public void init_UserDao() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        userDao = new UserDAO(mockJdbc);
        user = new User("user", null, null, null);
    }

    @Test
    // not a part of test suite
    public void test_Change_Null() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            UserDAO.Change(null);
        });
    }

    @Test
    public void test_Change_Nothing() {
        UserDAO.Change(user);
        Mockito.verify(mockJdbc, Mockito.never())
                .update(Mockito.anyString(), Mockito.anyCollection());
    }

    @Test
    public void test_Change_Email() {
        user.setEmail("new_mail@new_mail.new");
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(
                "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;",
                user.getEmail(), user.getNickname());
    }

    @Test
    public void test_Change_Fullname() {
        user.setFullname("User the First");
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(
                "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;",
                user.getFullname(), user.getNickname());
    }

    @Test
    public void test_Change_About() {
        user.setAbout("This user will be modified 8 times");
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(
                "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;",
                user.getAbout(), user.getNickname());
    }

    @Test
    public void test_Change_Email_and_Fullname() {
        user.setEmail("new_mail@new_mail.new");
        user.setFullname("User the First");
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(
                "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;",
                user.getEmail(), user.getFullname(), user.getNickname());
    }


    @Test
    public void test_Change_Fullname_and_About() {
        user.setFullname("User the First");
        user.setAbout("This user will be modified 8 times");
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(
                "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;",
                user.getFullname(), user.getAbout(), user.getNickname());
    }

    @Test
    public void test_Change_Email_and_About() {
        user.setEmail("new_mail@new_mail.new");
        user.setAbout("This user will be modified 8 times");
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(
                "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;",
                user.getEmail(), user.getAbout(), user.getNickname());
    }

    @Test
    public void test_Change_Email_and_Fullname_and_About() {
        user.setEmail("new_mail@new_mail.new");
        user.setFullname("User the First");
        user.setAbout("This user will be modified 8 times");
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(
                "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;",
                user.getEmail(), user.getFullname(), user.getAbout(), user.getNickname());
    }


    @AfterEach
    public void destroy_UserDao() {
        mockJdbc = null;
        userDao = null;
    }
}
